import Home from './components/Home.vue';
import Movie from './components/Movie.vue';
import MovieItem from './components/MovieItem.vue';
import MovieUpdate from './components/MovieUpdate.vue';
import ShowMore from './components/ShowMore.vue';
import MovieNote from './components/MovieNote.vue';


const routes = [
    { path: '/', component: Home },
    { path: '/Movie', component: Movie },
    { path: '/MovieItem', component: MovieItem },
    { path: '/Movie/:id', name: 'movie', component: ShowMore },
    { path: '/Movie/:id/edit', name: 'edit', component: MovieUpdate },
    { path: '/Movie/:id/note', name: 'note', component: MovieNote },

];

export default routes;