import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

import routes from './routes';

Vue.config.productionTip = false;

Vue.use(VueRouter);

const router = new VueRouter({ routes });

new Vue({
    router,
    render: h => h(App),
    vuetify: new Vuetify(),
}).$mount('#app');

window.shared_data = {
    movies: [{
            id: '1',
            title: 'la ligne verte',
            years: '1999',
            language: 'Anglais',
            type: 'Drame/Crime',
            note: [],
            poster: "https://fr.web.img4.acsta.net/r_1920_1080/medias/nmedia/18/66/15/78/19254683.jpg",
            details: {
                director: 'Frank Darabont',
                nationality: 'France',
                date_of_birth: '28 janvier 1959',
                synopsis: 'Paul Edgecomb, pensionnaire centenaire d\'une maison de retraite, est hanté par ses souvenirs. Gardien-chef du pénitencier de Cold Mountain, en 1935, en Louisiane, il était chargé de veiller au bon déroulement des exécutions capitales au bloc E...'
            }
        }, {
            id: '2',
            title: 'interstellar',
            years: '2014',
            language: 'Anglais',
            type: 'SF/Aventure',
            note: [],
            poster: 'https://fr.web.img3.acsta.net/r_1920_1080/img/e7/aa/e7aa3aafba98ec994c06794f81c50c00.jpg',
            details: {
                director: 'Christopher Nolan',
                nationality: 'Americain',
                date_of_birth: '30 juillet 1970',
                synopsis: 'Dans un futur proche, la Terre est de moins en moins accueillante pour l humanité qui connaît une grave crise alimentaire.Le film raconte les aventures d un groupe...'
            }
        }, {
            id: '3',
            title: 'la grande vadrouille',
            years: '1966',
            language: 'Francais',
            type: 'Comédie/Guerre',
            note: [],
            poster: 'https://fr.web.img5.acsta.net/r_1920_1080/pictures/16/06/16/12/01/072037.jpg',
            details: {
                director: 'Gérard Oury',
                nationality: 'Francais',
                date_of_birth: '29 avril 1919',
                synopsis: 'En 1942, un avion anglais est abattu par les Allemands au-dessus de Paris. Les trois pilotes sautent en parachute et atterrissent dans différents endroits de la capitale...'
            }
        },
        {
            id: '4',
            title: 'forrest gump',
            years: '1994',
            language: 'Americain',
            type: 'Drame/Romance',
            note: [],
            poster: "https://fr.web.img6.acsta.net/r_1920_1080/pictures/bzp/01/10568.jpg",
            details: {
                director: 'Robert Zemeckis',
                nationality: 'Americain',
                date_of_birth: '14 mai 1952',
                synopsis: 'Au fil des différents interlocuteurs qui viennent s\'asseoir tour à tour à côté de lui sur un banc, Forrest Gump raconte la fabuleuse histoire de sa vie. Sa vie est à l\'image d\'une plume qui se laisse porter par le vent, tout comme Forrest se laisse porter par les événements...'
            }
        },
        {
            id: '5',
            title: 'american history X',
            years: '1999',
            language: 'Americain',
            type: 'Drame/Crime',
            note: [],
            poster: 'https://fr.web.img2.acsta.net/r_1920_1080/medias/03/26/20/032620_af.jpg',
            details: {
                director: 'Tony Kaye',
                nationality: 'Anglais',
                date_of_birth: '8 juillet 1952',
                synopsis: 'Ce film tente d\'expliquer l\'origine du racisme et de l\'extrémisme aux États-Unis. Il raconte l\'histoire de Derek qui, voulant venger la mort de son père, abattu par un dealer noir, a épousé les thèses racistes d\'un groupuscule de militants d\'extrême droite...'
            }
        },
        {
            id: '6',
            title: 'avatar',
            years: '2009',
            language: 'Americain',
            type: 'SF/Cinéma de fantasy',
            note: [],
            poster: 'https://fr.web.img5.acsta.net/r_1920_1080/img/65/ab/65ab47663c7c41e4e5039e34b800292b.jpg',
            details: {
                director: 'James Cameron',
                nationality: 'Canadien',
                date_of_birth: '16 août 1954',
                synopsis: 'Malgré sa paralysie, Jake Sully, un ancien marine immobilisé dans un fauteuil roulant, est resté un combattant au plus profond de son être. Il est recruté pour se rendre à des années-lumière de la Terre, sur Pandora...'
            }
        },
        {
            id: '7',
            title: 'dumb and dumber',
            years: '1994',
            language: 'Americain',
            type: 'Comédie',
            note: [],
            poster: 'https://fr.web.img2.acsta.net/r_1920_1080/img/27/ef/27ef6605710b38044bc7e65dc0802903.jpg',
            details: {
                director: 'Peter Farrelly',
                nationality: 'Americain',
                date_of_birth: '17 décembre 1956',
                synopsis: 'Chauffeur d\'une voiture de luxe, Lloyd conduit Mary à l\'aéroport. La belle oublie sa mallette dans le véhicule. Lloyd s\'en empare au nez et à la barbe d\'un sinistre couple qui tentait de se l\'accaparer. Lloyd persuade son ami et colocataire Harry...'
            }
        },
        {
            id: '8',
            title: 'green Book : Sur les routes du Sud',
            years: '2018',
            language: 'Americain',
            type: 'Drame/Crime',
            note: [],
            poster: 'https://fr.web.img5.acsta.net/r_1920_1080/img/9a/4b/9a4b1422bb6e9f595817a58689b41c50.jpg',
            details: {
                director: 'Peter Farrelly',
                nationality: 'Americain',
                date_of_birth: '17 décembre 1956',
                synopsis: 'En 1962, alors que règne la ségrégation, Tony Lip, un videur italo-américain du Bronx, est engagé pour conduire et protéger le Dr Don Shirley, un pianiste noir de renommée mondiale, lors d\'une tournée de concerts. Durant leur périple de Manhattan jusqu\'au Sud profond...'
            }
        }
    ]
}